/**
 * Created by apple on 08/07/17.
 */

/**
 * Closure for generating id for new tasks.
 */
var getId = (function () {
  var id = 0;
  return function () {return id += 1;}
})();

// region GLOBAL VARIABLES
var tasks = [];
var count = {
  'todo': 0,
  'in-progress':0,
  'done':0,
};
var lastDelete = {};
var restoreFlag = false;
// endregion

// region Task Functions

function updateTask(taskId, previousState, nextState) {
  if(previousState !== nextState){
    var index = _.findIndex(tasks, function (t) {
      return t.id == taskId;
    });

    tasks[index].state = nextState;
    changeCount(previousState, nextState);
  }
}

function deleteTask(id) {
  var index = _.findIndex(tasks, function (t) {
      return t.id == id;
  });
  lastDelete = tasks[index];
  count[lastDelete.state]--;
  tasks.splice(index,1);
  $(`#${id}`).remove();
  updateCounts();
  restoreFlag = true;
  showMessage('Task removed.');
}

function restoreDelete() {

  if(restoreFlag){
    tasks.push(lastDelete);
    count[lastDelete.state]++;
    $(`#${lastDelete.state}`).append(`<li id="${lastDelete.id}">${lastDelete.text}<span class="glyphicon glyphicon-trash pull-right" onclick="deleteTask(${lastDelete.id})"></span></li>`);
    updateCounts();
    restoreFlag = false;
    lastDelete = {};
    showMessage('Task restored');
  }

}

function changeCount(previousState, nextState) {
  count[previousState]--;
  count[nextState]++;
  updateCounts();
}

function updateCounts() {
  $('#todo-count').html(count.todo);
  $('#in-progress-count').html(count['in-progress']);
  $('#done-count').html(count.done);
  $('#total-count').html(count.done+count['in-progress']+count.todo);
}

function showMessage(message) {
  $('#message').html(message).addClass('show');
  setTimeout(function () {
    $('#message').removeClass('show');
  }, 3000);
}

// endregion

// region Events
$("#create-task").on('keyup', function (e) {

  if (e.keyCode === 13) {
    var taskHeading = $(this).val();

    if(taskHeading !== ""){
      var newTask = {
        id: getId(),
        text: taskHeading,
        state: 'todo',
      }; // new Task Object.
      tasks.push(newTask);
      $('#todo').append(`<li id="${newTask.id}">
                            ${newTask.text}
                            <span class="glyphicon glyphicon-trash pull-right" onclick="deleteTask(${newTask.id})"></span>
                          </li>`); // Appending New Task to the To-do Container.
      count.todo++; // Always add new task to the To-do container.
      updateCounts();
      $(this).val(''); // Reset input to initial value.
      showMessage('New task added.');
    }
  }

});

$(document).ready(function () {
  $( "#todo, #in-progress, #done" ).sortable({ // Initialising Sortable lists.
    connectWith: ".connectedSortable",
    receive: function( event, ui ) { // Event when a list is placed to any other position.
      updateTask(ui.item[0].id, ui.sender[0].id, ui.item[0].parentElement.id);
    }
  }).disableSelection();
});

// endregion